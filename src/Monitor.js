
import async from 'async';
import bunyan from 'bunyan';
import lodash from 'lodash';
import yaml from 'js-yaml';

import Git from './Git';
import Ingester from './Ingester';

const log = bunyan.createLogger({name: 'monitor'});

module.exports = {
   start() {
      log.info('start', { seconds: global.config.monitorSeconds });
      monitor();
      setInterval(monitor, global.config.monitorSeconds*1000);
   }
};

function monitor() {
   log.debug('monitor');
   Git.latestCommit(commit => {
      if (!exports.commit) {
         log.info('commit init', commit.sha);
         exports.commit = commit;
         Ingester.start();
      } else if (commit.sha !== exports.commit.sha) {
         log.info('commit changed', commit.sha);
         exports.commit = commit;
         Ingester.changed();
      } else {
         log.debug('commit unchanged', commit.sha);
      }
   });
}
