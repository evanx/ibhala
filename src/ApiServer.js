'use strict';

import async from 'async';
import lodash from 'lodash';
import express from 'express';
import bunyan from 'bunyan';
import yaml from 'js-yaml';
import redisModule from 'redis';
import fs from 'fs';
import path from 'path';
import marked from 'marked';

const app = express();
const redisClient = redisModule.createClient();
const log = bunyan.createLogger({name: 'apiserver'});
const config =  yaml.safeLoad(fs.readFileSync('config/default.yaml', 'utf8'));

redisClient.on('error', err => {
   log.error('error', err);
});

global.redisClient = redisClient;
global.log = log;
global.config = config;
global.data = {
   feeds: {},
   articles: {},
   images: {}
}
import Monitor from './Monitor';

global.bus = {
   image(image) {
      image.id = path.basename(image.uri.toLowerCase());
      log.info('image', image);
      global.data.images[image.id] = {
         id: image.id,
         uri: image.uri,
         caption: image.caption,
         credit: image.credit,
         content: fs.readFileSync('.' + image.uri)
      };
      return image.id;
   },
   article(article) {
      global.data.articles[article.contentKey] = article;
      article.images.forEach(image => {
         article.imageId = this.image(image);
         article.imageUrl = 'http://localhost:8810/image/wow/560n/' + article.imageId;
      });
   },
   feed(feed) {
      global.data.feeds[feed.name] = feed;
   }
}

function handleError(res, error) {
   log.error('error', error);
   if (error instanceof Error) {
      log.error('error stack', error.stack);
   }
   res.status(500).send(error);
}

function getHelp(req, res) {
   try {
      res.set('Content-Type', "text/html");
      fs.readFile('README.md', function (err, content) {
         if (content) {
            res.send(marked(content.toString()));
         } else {
            res.send('no help');
         }
      });
   } catch (error) {
      handleError(res, error);
   }
}

function appLogger(req, res, next) {
   log.info('app', req.url);
   next();
}

function start() {
   app.use(appLogger);
   app.get('/help', getHelp);
   app.get('/feeds', getFeeds);
   app.get('/feed/:id', getFeedId);
   app.get('/article/:id', getArticleId);
   app.get('/a/:id', renderArticleId);
   app.get('/image/:folder/:size/:id', getImageId);
   app.get('/imageinfo/:folder/:size/:id', getImageInfoId);
   app.listen(global.config.port);
   log.info('started', {port: global.config.port});
   Monitor.start();
}

function getFeeds(req, res) {
   try {
      res.json(Object.keys(global.data.feeds));
   } catch (err) {
      res.status(500).send(err);
   };
}

function getFeedId(req, res) {
   try {
      res.json(global.data.feeds[req.params.id]);
   } catch (err) {
      res.status(500).send(err);
   };
}

function getArticleId(req, res) {
   try {
      res.json(global.data.articles[req.params.id]);
   } catch (err) {
      res.status(500).send(err);
   };
}

function renderArticleId(req, res) {
   try {
      req.article = global.data.articles[req.params.id];
      require('./Article').render(req, res);
   } catch (err) {
      res.status(500).send(err);
   };
}

function getImageInfoId(req, res) {
   try {
      let image = global.data.images[req.params.id.toLowerCase()];
      res.json({
         width: 560,
         height: 315,
         uri: image.uri,
         caption: image.caption,
         credit: image.credit,
      });
   } catch (err) {
      res.status(500).send(err);
   };
}

function getImageId(req, res) {
   try {
      res.contentType('image/jpeg');
      res.send(global.data.images[req.params.id.toLowerCase()].content);
   } catch (err) {
      res.status(500).send(err);
   };
}

start();
