
import async from 'async';
import bunyan from 'bunyan';
import lodash from 'lodash';
import yaml from 'js-yaml';
import fs from 'fs';
import path from 'path';

import ArticleIngester from './ArticleIngester';

const log = bunyan.createLogger({name: 'FeedIngester'});

function FeedIngester(feedFile) {
   try {
      let feedInfo = yaml.safeLoad(fs.readFileSync(feedFile, 'utf8'));
      log.info('feedInfo', { label: feedInfo.label, articlesLength: feedInfo.articles.length });
      let feed = {
         name: path.basename(feedFile, '.yaml'),
         label: feedInfo.label,
         articles: []
      }
      lodash.forEach(feedInfo.articles, articleInfo => {
         let article = new ArticleIngester(articleInfo).getJson();
         feed.articles.push({
            contentKey: article.contentKey,
            headline: article.headline,
            abstract: article.abstract,
            imageUri: article.images[0].uri
         });
         global.bus.article(article);
      });
      global.bus.feed(feed);
   } catch (error)  {
      log.error(error);
   }
};

module.exports = FeedIngester;
