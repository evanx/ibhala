
import React from 'react';
import bunyan from 'bunyan';
import lodash from 'lodash';

const log = bunyan.createLogger({name: 'Article'});

var Article = React.createClass({
   render: function() {
      let article = this.props.article;
      return (
         <div className="articleContainer">
            <h1 style={{marginTop: 4}}>{article.headline}</h1>
            <img src={article.imageUrl}/>
            <p dangerouslySetInnerHTML={{__html: article.bodyHTML}}></p>
         </div>
      );

   }
});

function render(req, res) {
   log.info('render', req.article);
   var html = React.renderToString(
      React.createElement(Article, {article: req.article}));
   res.set('Content-Type', 'text/html');
   res.send(html);
}

module.exports = { render }
