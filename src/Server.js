'use strict';

import async from 'async';
import lodash from 'lodash';
import express from 'express';
import bunyan from 'bunyan';
import yaml from 'js-yaml';
import redisModule from 'redis';
import fs from 'fs';
import marked from 'marked';

const app = express();
const redisClient = redisModule.createClient();
const log = bunyan.createLogger({name: 'apiserver'});
const config =  yaml.safeLoad(fs.readFileSync('config/default.yaml', 'utf8'));

redisClient.on('error', err => {
   log.error('error', err);
});

global.redisClient = redisClient;
global.log = log;
global.config = config;
global.data = {
   feeds: {},
   articles: {},
   images: {}
}
import Monitor from './Monitor';

global.bus = {
   article(article) {
      global.data.articles[article.contentKey] = article;
   },
   feed(feed) {
      global.data.feeds[feed.name] = feed;
   },
   image(image) {
      global.data.images[image.id] = image;
   }
}

function handleError(res, error) {
   log.error('error', error);
   if (error instanceof Error) {
      log.error('error stack', error.stack);
   }
   res.status(500).send(error);
}

function getHelp(req, res) {
   try {
      res.set('Content-Type', "text/html");
      fs.readFile('README.md', function (err, content) {
         if (content) {
            res.send(marked(content.toString()));
         } else {
            res.send('no help');
         }
      });
   } catch (error) {
      handleError(res, error);
   }
}

function appLogger(req, res, next) {
   log.info('app', req.url);
   next();
}

function start() {
   app.use(appLogger);
   app.get('/help', getHelp);
   app.get('/feeds', getFeeds);
   app.get('/feed/:id', getFeedId);
   app.get('/article/:id', getArticleId);
   app.get('/image/:id', getImageId);
   app.listen(global.config.port);
   log.info('started', {port: global.config.port});
   Monitor.start();
}

function getFeeds(req, res) {
   try {
      res.json(Object.keys(global.data.feeds));
   } catch (err) {
      res.status(500).send(err);
   };
}

function getFeedId(req, res) {
   try {
      res.json(global.data.feeds[req.params.id]);
   } catch (err) {
      res.status(500).send(err);
   };
}

function getArticleId(req, res) {
   try {
      res.contentType()
      res.json(global.data.articles[req.params.id]);
   } catch (err) {
      res.status(500).send(err);
   };
}

function getArticleId(req, res) {
   try {
      res.contentType()
      res.json(global.data.articles[req.params.id]);
   } catch (err) {
      res.status(500).send(err);
   };
}

start();
