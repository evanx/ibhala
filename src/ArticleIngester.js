
import async from 'async';
import bunyan from 'bunyan';
import lodash from 'lodash';
import path from 'path';
import yaml from 'js-yaml';
import fs from 'fs';
import Promise from 'es6-promises';

const log = bunyan.createLogger({name: 'ArticleIngester'});


class ArticleIngester {
   constructor(articleInfo) {
      this.articleInfo = articleInfo;
      this.lineCount = 0;
      this.paragraphs = [];
      try {
         log.debug('articleInfo.content', articleInfo.contentFile);
         this.lines = fs.readFileSync(articleInfo.contentFile).toString().split('\n');
         this.parseLines();
         let article = this.getJson();
         log.info('article', article);
      } catch (error)  {
         log.error(articleInfo.contentFile, this.lineCount, this.line, error);
         log.error(error.stack);
      }
   }

   getJson() {
      return {
         contentKey: path.basename(this.articleInfo.contentFile, '.md'),
         headline: this.headline,
         abstract: this.abstract,
         bodyHTML: this.getBodyHtml().join('\n'),
         featuredImageUri: this.imageUri,
         images: [
            {
               uri: this.imageUri,
               caption: this.imageCaption,
               credit: this.imageCredit
            }
         ]
      };
   }

   getBodyHtml() {
      return lodash.map(this.paragraphs, paragraph => {
         return '<p>' + paragraph + '</p>';
      });
   }

   parseLines() {
      log.info('lines', this.lines.length);
      this.lines.forEach(line => {
         this.line = line;
         if (line.indexOf('# ') === 0) {
            this.headline = line.substring(2);
            log.info('headline:', this.headline);
         } else if (line.indexOf('## By: ') === 0) {
            this.byline = line.substring(7);
            log.info('byline:', this.byline);
         } else if (line.indexOf('### Abstract: ') === 0) {
            this.abstract = line.substring(14);
            log.info('abstract:', this.abstract);
         } else if (line.indexOf('![') === 0) {
            let fromIndex = line.indexOf('(');
            let toIndex = line.lastIndexOf(')');
            if (fromIndex > 0 && toIndex > fromIndex) {
               this.imageUri = line.substring(fromIndex + 1, toIndex);
               log.info('image:', this.imageUri);
            } else {
               throw new Error();
            }
            this.parsingImage = true;
         } else if (line.indexOf('#### Caption: ') === 0) {
            if (this.parsingImage) {
               let creditIndex = line.indexOf(' Credit: ');
               if (creditIndex > 0) {
                  this.imageCaption = line.substring(14, creditIndex);
                  this.imageCredit = line.substring(creditIndex + 9);
                  log.info('imageCredit', this.imageCredit);
               } else {
                  this.imageCaption = line.substring(14);
               }
               log.info('imageCaption', this.imageCaption);
            } else {
               throw new Error('Invalid caption line');
            }
         } else if (line.indexOf('- ') === 0) {
            log.info('dashed', line);
         } else if (line.match(/^\s*$/)) {
         } else if (line.indexOf('#') === 0) {
            throw new Error('Invalid info line');
         } else {
            this.parsingImage = false;
            this.paragraphs.push(line);
            log.debug('line', this.lineCount, line);
         }
         this.lineCount += 1;
      });
   }
}

module.exports = ArticleIngester;
