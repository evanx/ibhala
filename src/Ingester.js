
import async from 'async';
import bunyan from 'bunyan';
import lodash from 'lodash';
import yaml from 'js-yaml';
import fs from 'fs';

import FeedIngester from './FeedIngester';

const log = bunyan.createLogger({name: 'Ingester'});

function ingest() {
   try {
      log.info('ingest');
      let manifest = yaml.safeLoad(fs.readFileSync('manifest.yaml', 'utf8'));
      log.info('feeds', manifest.feeds);
      lodash.forEach(manifest.feeds, FeedIngester);
   } catch (error) {
      log.error(error);
   }
}

function start() {
   log.info('start');
   ingest();
}

function changed() {
   log.info('changed');
   ingest();
}

module.exports = { start, changed };
