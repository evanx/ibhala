
import nodegit from 'nodegit';

module.exports = {
   latestCommit(callback) {
      nodegit.Repository.open(".").then(repo => {
         return repo.getMasterCommit();
      }).then(masterCommit => {
         var history = masterCommit.history();
         var count = 0;
         history.on("commit", function(commit) {
            if (count === 0) {
               callback({
                  sha: commit.sha(),
                  date: commit.date(),
                  message: commit.message(),
                  authorName: commit.author().name()
               });
            }
            count += 1;
         });
         history.start();
      });
   }
};

if (false) {
   module.exports.latestCommit(commit => {
      console.info('latestCommit', commit);
   });
}
