
var nodegit = require("nodegit");
var path = require("path");

var resolvedPath = path.resolve(__dirname, "../.git");
console.info('resolvedPath', resolvedPath);

nodegit.Repository.open(path.resolve(__dirname, "../.git"))
  .then(function(repo) {
    console.info('repo', repo);
    return repo.fetch("origin", {
      credentials: function(url, userName) {
        console.info('credentials', userName);
        return nodegit.Cred.sshKeyFromAgent(userName);
      }
    });
  }).done(function() {
    console.log("It worked!");
  });
