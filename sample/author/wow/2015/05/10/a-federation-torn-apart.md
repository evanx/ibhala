
# A federation torn apart

## By: Oupa Bodibe

### Abstract: No one in Cosatu could have foreseen the current state of the federation when it adopted the 2015 Plan over 10 years ago. This year marks 30 years since the formation of Cosatu and the Plan was adopted to coincide with this milestone.

No one in Cosatu could have foreseen the current state of the federation when it adopted the 2015 Plan over 10 years ago. This year marks 30 years since the formation of Cosatu and the Plan was adopted to coincide with this milestone.

In terms of this blueprint, Cosatu would have doubled its membership and workers would have been at the centre of the national agenda. The second decade of freedom was supposed to benefit mostly working-class families.

![](/sample/images/wow/560n/WOW_Cosatu_logo1.jpg)

#### Caption: Cosatu logo.

It is now common knowledge that the federation’s future is uncertain. The biggest union, Numsa, has been expelled, a number of unions have suspended participation in the central executive committee and the general secretary has been expelled for misconduct.

Personally this is very painful to observe, having worked for the federation for 11 years. It is like observing an acrimonious divorce between parents while hoping somehow sanity will prevail and the split avoided. My firm belief is that democracy is poorer with a fragmented labour movement.

If it is any consolation, trade union rifts are not peculiar to South Africa, nor are they new. The US trade union federation, the AFL-CIO, split several years ago and has not fully recovered. In addition, trade unions linked to political parties are typically not spared the ructions engulfing the political party. In some cases, tension arises around the political direction that the party takes especially when it is unpopular with union members.

The tension between the Labour Party and the British Trade Union Congress, the Labour Party and LO Norway, ZCTU and Zanu in Zimbabwe, and ZCTU and MDC, are just some examples.

Still, the current tension and challenges do not leave room for complacency. Where to for Cosatu and labour as a whole? A number of scenarios are more than likely.

The first scenario is Cosatu as a divided house. In this scenario no actual split takes place, but internally there is no coherence or unity. The line of division is more or less the current alignment.

The second scenario is an actual split along the same lines of alignment. The third option is the emergence of a new federation led or inspired by Numsa working with some of the other unions that would have walked out of Cosatu.

My preferred option is for a solution that can reunify the federation with a new commitment to working together to advance the cause of the workers. In the current climate we have to ask if this is feasible and what the conditions are that can make it possible.

This will be a high mountain to climb and will require finding middle ground acceptable to all on issues in contestation.

That said, there are several implications that flow from a disunited Cosatu. Undoubtedly the workplace is going to be a “hot place” as unions try to displace and out-manoeuvre each other to gain ascendancy and members. Union organising is a numbers game and each of them will vie to become the majority union. The contestation in the platinum belt between the National Union of Mineworkers (NUM) and the Association of Mineworkers and Construction Union (Amcu) comes to mind. In that situation Amcu replaced the NUM as the majority union in Lonmin and the unions are battling it out for supremacy.

As unions poach and try to outrecruit each other, violence lurks somewhere in the background and contestation might become openly vicious. Hopefully peaceful coexistence and co-operation will be the main mode of working among unions. Many unions have learnt to coexist and sometimes co-operate in a number of sectors. The NUM, Solidarity and Numsa peacefully coexisted for a long time in Eskom and other companies. Transnet is another example of multiple unions coexisting amicably, but this is perhaps because they represent different layers of the workforce.

Union rivalry is not necessarily a bad thing and may actually be of benefit to members.

Although unions are founded on the principle of solidarity, a little competition might be good for members. It might compel unions to improve services to members.

This might also induce innovation in union organising and services to try to retain existing members and recruit new ones.

Another question is: Will the current climate actually increase union membership overall, or will it be a situation of unions cannibalising each other without altering the proportion of union members in the formal sector?

If overall union density (the proportion of union members relative to formal employment) is not improved, fragmentation will only serve to reallocate members among the unions while a substantial part of the formal sector workers will remain un-organised. Unions battle constantly to recruit casual or atypical workers.

Overall union density for the past 20 years stabilised at below 30 percent of formal sector employment. Sectors with high union density are the public sector and mining while services have low union density.

Another dimension is: How will this affect industrial relations? On this front, the temperature is likely to rise as unions will have to out-compete each other and project a more militant stance. It is more than likely that union demands will become high and bargaining a lot harder. An employer dealing with multiple unions with different approaches is likely to face an unstable negotiation terrain.

Ultimately it will depend on union strength to force the hand of the employer to wrest some concessions. But a divided labour movement is unlikely to muster enough power to gain ground against employers.

In the event of a split and emergence of a new federation, the ANC-led alliance will have to contend with a new force on the left.

Numsa has made it clear that it is consciously forming new alliances with the view to contesting elections.

It will be interesting how this affects the posture and tone of the ANC-led alliance on key development questions. Will the ANC simply dismiss these new voices on the left confident that it has not really confronted anything likely to unseat it, or will it change its stance? Regardless of how the situation in Cosatu pans out, there is no doubt that the trade union landscape will no longer be the same.

- Oupa Bodibe is a former Cosatu researcher.
