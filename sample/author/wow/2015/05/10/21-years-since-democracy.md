
# 21 years since democracy

## By: Adam Habib

### Abstract: Democracy is essentially about accountability and about making political elites responsive to their citizens. It does not assume that responsiveness to citizens is a product of good politicians.

Democracy is essentially about accountability and about making political elites responsive to their citizens. It does not assume that responsiveness to citizens is a product of good politicians.

Rather it assumes that this may not always be the case, and establishes institutional mechanisms that act as a check on elected leaders.

Some of these mechanisms, such as opposition parties and regular elections, are meant to create a level of political uncertainty for political leaders, prompting them to win over citizens by becoming responsive to their concerns.

![](/sample/images/wow/560n/wow_texttile.jpg)

#### Caption: Workers in the textile industry. Credit: Independent

Others, like the judiciary and the public protector, are meant to ensure that all comply with the collectively determined social compact, the constitution, and subsequently with the legislation enacted in terms of it.

How well these institutions do this is a measure of the strength of our democracy.

Responses from government to the recent release of the |Nkandla report by the public protector’s office are a stark reminder that more than 21 years on, we still have a long way to go |in this regard.

On the economic front, the widespread cynicism of both the political and economic elites has not gone unnoticed in the world.

Foreign investors have become increasingly sceptical of South Africa’s economic prospects, as have rating agencies, especially because of the political instability emanating from the widespread resentment in the society.

Direct investment has plummeted, as have growth rates, making manifest our day of reckoning even to the most politically oblivious corporate economists in the country.

Yet we are not predestined to make bad choices.

It is possible for our political and economic elites to transcend their narrow short-term interests, and make decisions that could place us on the path to an economically sustainable, more inclusive, and less fractious society.

Such decisions would require the establishment of a social democratic political economy, which is a necessary prerequisite for South Africa, to comprehensively deal with the challenges of inequality, poverty and unemployment.

Although this was acknowledged at the ANC’s national conferences in 2007 and 2012, and some elements of a social democratic platform have gradually been adopted, this economic perspective is still constantly challenged within the ruling party by individuals and factions that advocate fiscal conservatism in the guise of financial prudence.

This battle for the economic soul of the ANC has to be resolved. Related to this is the need for a new social pact to be cemented between business, labour and the state.

However, the legitimacy crisis of the ruling party has provoked widespread factionalism within the ANC and within the Tripartite Alliance.

This factionalism was concentrated within the ANC in the build-up to Mangaung.

In the aftermath of President Jacob Zuma’s decisive victory in Mangaung, the factionalism shifted to Cosatu.

The focus of this factionalism has revolved around the expulsion of Zwelinzima Vavi from the union federation because of his criticisms of, and independence from, the Zuma administration.

Initially the expulsion was attempted at the Cosatu congress, but his grass-roots popularity prevented this.

Subsequently, attempts were made to subject him to corruption charges primarily around the sale of Cosatu’s previous headquarters.

When this seemed to go nowhere, Vavi’s opponents opportunistically latched onto his sexual indiscretions, and had him expelled  from the federation.

The move has paralysed the union federation and its affiliates at a time when it confronts its greatest threats at the grass-roots level.

There is widespread suspicion, including in the Vavi camp, that |the attempts to expel the outspoken general secretary received support from the highest office in the |land.

Ironically then, the Zuma Presidency in the ANC has been able to effect on its ally what apartheid and corporate South Africa have failed to do in almost three decades: achieve a political and administrative paralysis of the largest union federation.

Yet corporate South Africa is also not immune from the legitimacy crisis.

The protests and massacre in Marikana have focused the public glare on corporate South Africa as much as it has on the Zuma administration.

The enrichment of corporate executives who earn multimillion-rand packages has not only generated widespread cynicism among the citizenry, but has also provoked organised workers to demand between 60 percent and 120 percent salary increases.

Marikana has also cast a public glare on the close collusion between corporate South Africa and ANC politicians, manifested in black enrichment schemes captured most dramatically in Cyril Ramaphosa’s shareholding, in Lonmin itself.

The approach of our day of reckoning is increasingly obvious to all but the most blinded.

Yet as I have indicated earlier, we are not predestined to make bad choices.

However the Zuma administration has failed to manage the expectations of |both the economic elite and the general populace, with the result that an essential foundation for a successful pact – a willingness by |all to defer the immediate realisation of their desires – has |not been achieved.

This is a profound failure of political management.

At one level, this has been recognised by some political leaders, and in particular by former finance minister Pravin Gordhan, who has spoken out against excessive executive remuneration and enrichment in both the private and public sectors.

At another level, the lesson has not been truly internalised.

Leave aside the irony of a whole range of individuals, mainly from the political and economic elites, professing that they represent the true interests of the unemployed, and lecturing unions on their selfishness, when it is union members who live and interact on a daily basis with those who are truly marginalised.

Should we not be concerned that this diverse set of leaders with their impressive track records and experience have not yet understood the basis of successful social pacts? Pacts require compromise.

A central dilemma that a South African social pact would have to address is how to enable economic competitiveness and the accumulation of work experience by new entrants to the labour market without losing the hard-won gains of the labour movement as expressed in the Labour Relations Act of 1995.

The unions fear that compromises might weaken the act, and that this would allow employers to roll back the gains won by workers in the formal sector.

There is a precedent for this in the post-apartheid era, and to argue that this is impossible and unrealistic is to be out of touch with the economic dynamics of the past 20 years.

For example, in the mid-1990s, internal employees carried out cleaning services.

Since then, most private companies and public institutions have subcontracted this to external companies that employ workers at much lower wages and provide far fewer benefits.

Similarly, a study on transformation in South African mines demonstrated that as the racial ownership of mines had changed, so working conditions had worsened.

Essentially, South Africa’s more marginal mines have been sold to black entrepreneurs who have derived profits from squeezing wages and reducing benefits.

This has been achieved mainly through the use of labour brokers, which have served as a “safety valve” for business owners, enabling them to avoid the obligations made mandatory by the Labour Relations Act.

But business is also correct to insist on the recognition that South Africa faces significant economic challenges.

Many economic sectors are not globally competitive, and excessive red tape compromises the viability of many small enterprises.

Moreover, given the staggering rate of unemployment among young people, there is an urgent need to create an environment in which employers are willing to take on new entrants into the labour market so that they can accumulate the necessary work experience to make them valuable and productive employees.

These challenges should be of as much concern to labour as they are to employers and the state.

The test for potential partners in a social pact is to create the conditions for addressing these problems without sacrificing or weakening the protections afforded to formal sector employees.

With some imagination, this can be done.

For example, agreements entered into with employers in the textile sector in Newcastle in October 2011 involved wage concessions for new employees.

This suggests that unions can be pragmatic when it comes to ensuring company competitiveness and survival.

Unions may have to compromise on a wage subsidy for new and young employees, and allow for special wage and employment provisions for certain economic sectors or geographic zones.

Similarly, employers may have to commit to employment targets in these sectors, and provide guarantees that they will not try to generalise these special provisions to the broader labour market.

In other words, the fears of all parties must be addressed, and in this process, compromises are required from, and costs need |to be borne by, both business |and labour.

As long as an equitable social pact remains a distant dream, South Africa will not succeed in bridging the divide between economic growth and inclusive development, or be able to address the related polarisation and social pathologies – chronic unemployment, child and woman abuse, substance abuse, violent crime – that characterise our society.

If we do not do this, we |would also not have responded positively to our moment of reckoning.

And if this does not happen, then we set the trajectory for an alternative evolution of a society; one where the future is not so promising.

- Adam Habib is the Vice-Chancellor and principal of the University of the Witwatersrand and author of South Africa’s Suspended Revolution: Hopes and Prospects.

