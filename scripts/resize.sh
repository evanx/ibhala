

c3w() {
  dir=$1
  name=$2
  width=$3
  mkdir -p $dir/w$width
  curl -s "http://localhost:5060/w$width/http://localhost:8080/$dir/original/$name" -o "$dir/w$width/$name"
  ls -l $dir/w$width/$name
  identify -format "%[w]x%[h]" $dir/w$width/$name
}

c4crop() {
  dir=$1
  name=$2
  width=$3
  height=$4
  curl -s "http://localhost:5060/w$width/http://localhost:8080/$dir/original/$name" -o "$dir/w$width/$name" || return 
  ls -l $dir/w$width/$name
  identify -format "%[w]x%[h]" $dir/w$width/$name
  curl -s "http://localhost:5060/c${width}x${height}n/http://localhost:8080/$dir/w$width/$name" -o $dir/${width}n/$name || return
  ls -l $dir/${width}n/$name
  identify -format "%[w]x%[h]" $dir/${width}n/$name
}

c2resize() {
  dir=$1
  name=$2
  mkdir -p $dir/w320 $dir/320n $dir/w560 $dir/560n
  c4crop $dir $name 320 180
  c4crop $dir $name 560 315
}

c1resize() {
  dirname=$1
  for name in `ls sample/images/$dirname/original`
  do
    c2resize sample/images/$dirname $name
  done 
  find sample/images/$dirname
}

c1resize wow 

